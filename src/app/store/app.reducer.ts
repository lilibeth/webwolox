import { ActionReducerMap } from '@ngrx/store';
import { listReducer, ListState } from './list/reducer/list.reducer';
import { LoginState, loginReducer } from './login/reducer/login.reducer';


export interface AppState{
    login: LoginState;
    list: ListState;
  }

export const appReducers: ActionReducerMap<AppState> = {
    login: loginReducer,
    list: listReducer
};
