import * as fromList from '../actions/list.actions';
export interface ListState {
    loaded: boolean;
    loading: boolean;
    error: any;
    data: any;
}
export const estadoInicialList: ListState = {
    loaded: false,
    loading: false,
    error: null,
    data: null
};
export function listReducer( state = estadoInicialList, actions: fromList.listAcciones): ListState{
    switch (actions.type) {
        case fromList.CARGAR_LIST:
            return {
                ...state,
                loading: true,
                error: null
            };
        case fromList.CARGAR_LIST_SUCCESS:
            return {
                ...state,
                loading: false,
                loaded: true,
                data: actions.data,
            };
        case fromList.CARGAR_LIST_FAIL:
            return {
                 ...state,
                 loaded: false,
                 loading: false,
                 error: {
                     status: actions.payload.status,
                     message: actions.payload.message,
                     url: actions.payload.url

                 }
            };
        default:
            return state;
    }
}
