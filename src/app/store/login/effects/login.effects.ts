import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import * as loginActions from '../actions/login.actions';
import { switchMap, map, catchError } from 'rxjs/operators';
import { LoginService } from 'src/app/services/login/login.service';

@Injectable()
export class LoginEffects {
    constructor(private actions$: Actions, public loginService: LoginService){}
    @Effect()
    cargarLogin$ = this.actions$.pipe(
                         ofType( loginActions.CARGAR_LOGIN ),
                         switchMap( action => {
                            const key = 'data';
                            return this.loginService.getLogin(action[key]).pipe(
                                map( login =>  new loginActions.CargarLoginSuccess( login )),
                                catchError(Error => {
                                    let NError = Error;
                                    const keyError  = 'error';
                                    const keyErros = 'errors';
                                    if (Error[keyError] && Error[keyError][keyErros]){
                                     NError = Error[keyError][keyErros][0];
                                    }
                                    return of( new loginActions.CargarLoginFail( NError ));
                                }));
                            })
                        );
}

