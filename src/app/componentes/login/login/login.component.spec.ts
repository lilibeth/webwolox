import { HttpClient, HttpClientModule } from '@angular/common/http';
import { async, ComponentFixture, inject, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Route, Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { Actions } from '@ngrx/effects';
import { provideMockActions } from '@ngrx/effects/testing';
import { Store, StoreModule } from '@ngrx/store';
import { Observable, of } from 'rxjs';
import { TestStore } from 'src/app/mocks/teststore';
import { LoginService } from 'src/app/services/login/login.service';
import { AppState } from 'src/app/store/app.reducer';
import { LoginEffects } from 'src/app/store/login/effects/login.effects';
import { Location } from '@angular/common';

import { LoginComponent } from './login.component';
import { provideMagicalMock } from 'angular-testing-library/src/service_mock';
import { CargarLogin } from 'src/app/store/login/actions/login.actions';

describe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;
  let store: TestStore<AppState>;
  let effects: LoginEffects;
  let actions: Observable<any>;
  let queryService: LoginService;
  let httpClient: HttpClient;
  let router: Route;
  let location: Location;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoginComponent ],
      imports: [
        HttpClientModule,
        FormsModule,
        ReactiveFormsModule,
        StoreModule.forRoot({}),
        RouterTestingModule
      ],
      providers: [
        {
        provide: Store,
        useClass: TestStore,
        LoginService
      },
      LoginEffects,
      provideMockActions(() => actions),
      provideMagicalMock(LoginService)
      ]
    })
    .compileComponents();
    actions = TestBed.inject(Actions);
    effects = TestBed.inject(LoginEffects);
    queryService = TestBed.inject(LoginService);
  }));

  beforeEach(inject([Store], (testStore: TestStore<AppState>) => {
    store = testStore;
    store.setState({ data: [] });
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    router = TestBed.get(Router);
    location = TestBed.inject(Location);
    fixture.detectChanges();
    queryService = new LoginService(httpClient);
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('Deberia hacer el llamado a la action CargarLogin', () => {
    const DLogin = {
      email: 'prueba@gmai.com',
      password: '124568'
    };
    const action = new CargarLogin(DLogin);
    actions = of(action);
    const StoreR = TestBed.get(Store);
    const spy = spyOn(store, 'dispatch');
    StoreR.dispatch();
    component.login(DLogin);
    expect(spy).toHaveBeenCalled();
    expect(effects).toBeTruthy();
  });
});
