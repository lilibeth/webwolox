import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/store/app.reducer';
import { CargarLogin } from 'src/app/store/login/actions/login.actions';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  cargando: boolean;
  subscription;
  hide = true;
  menuActive = true;

  constructor(
             public store: Store<AppState>,
             private router: Router,
             private formBuilder: FormBuilder) {
    this.createFormLogin();
  }

  createFormLogin(): void {
    this.loginForm = this.formBuilder.group(
      {
        email: [null, [Validators.required, Validators.email]],
        password: [null, [Validators.required, Validators.minLength(8)]],
        remember: [false]
      }
    );
  }
  ngOnInit(): void { }

  login(value): void {
    this.store.dispatch(new CargarLogin(value));
    this.store.select('login').subscribe(login => {
      if (login.data != null) {
        if (value.remember === true){
          localStorage.setItem('remember', 'true');
        }
        this.router.navigate(['list']);
      }
    });
  }
  openMenu(): void{
     this.menuActive = !this.menuActive;
  }
}
