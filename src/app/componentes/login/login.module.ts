import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './login/login.component';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { loginEffectsArr } from 'src/app/store/login/effects';
import { EffectsModule } from '@ngrx/effects';
import { RememberServicesService } from 'src/app/services/guards/remember-services.service';

const routes: Routes = [
  { path: 'login', component: LoginComponent , canActivate: [RememberServicesService]},
];

@NgModule({
  declarations: [LoginComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    EffectsModule.forRoot(loginEffectsArr)
  ]
})
export class LoginModule { }
