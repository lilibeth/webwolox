import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.css']
})
export class LandingComponent implements OnInit {
  remember = false;
  activeLang = 'es';
  menuActive = true;
  constructor(
    private translate: TranslateService
  ) {
    this.translate.setDefaultLang(this.activeLang);
  }
  ngOnInit(): void {
    const remember = localStorage.getItem('remember');
    if (remember === 'true'){
      this.remember = true;
    }
  }
  siguenos(): any {
    window.open('https://twitter.com/Wolox', '_blank');
  }
  conocemas(): any {
    window.open('https://www.wolox.com.ar/', '_blank');
  }
  cambiarLenguaje(lang): void {
    this.activeLang = lang;
    this.translate.use(lang);
  }
  openMenu(): void{
    this.menuActive = !this.menuActive;
  }
}
