import { HttpClient, HttpClientModule } from '@angular/common/http';
import { async, ComponentFixture, fakeAsync, inject, TestBed, tick } from '@angular/core/testing';
import { Route, Router } from '@angular/router';
import { Observable, of } from 'rxjs';
import { TestStore } from 'src/app/mocks/teststore';
import { ListService } from 'src/app/services/list/list.service';
import { AppState } from 'src/app/store/app.reducer';
import { ListEffects } from 'src/app/store/list/effects/list.effects';
import { Location } from '@angular/common';
import { ListHerramientasComponent } from './list-herramientas.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Store, StoreModule } from '@ngrx/store';
import { RouterTestingModule } from '@angular/router/testing';
import { provideMockActions } from '@ngrx/effects/testing';
import { provideMagicalMock } from 'angular-testing-library/src/service_mock';
import { Actions } from '@ngrx/effects';
import { CargarList } from 'src/app/store/list/actions/list.actions';
import { LoginComponent } from '../../login/login/login.component';
import { ReiciarValores } from 'src/app/store/login/actions/login.actions';

describe('ListHerramientasComponent', () => {
  let component: ListHerramientasComponent;
  let fixture: ComponentFixture<ListHerramientasComponent>;
  let store: TestStore<AppState>;
  let effects: ListEffects;
  let actions: Observable<any>;
  let queryService: ListService;
  let httpClient: HttpClient;
  let router: Route;
  let location: Location;
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListHerramientasComponent , LoginComponent],
      imports: [
        HttpClientModule,
        FormsModule,
        ReactiveFormsModule,
        StoreModule.forRoot({}),
        RouterTestingModule.withRoutes([
          { path: 'login', component: LoginComponent }
        ])
      ],
      providers: [
        {
        provide: Store,
        useClass: TestStore,
        ListService
      },
        ListEffects,
      provideMockActions(() => actions),
      provideMagicalMock(ListService)
      ]
    })
    .compileComponents();
    actions = TestBed.inject(Actions);
    effects = TestBed.inject(ListEffects);
    queryService = TestBed.inject(ListService);
  }));
  beforeEach(inject([Store], (testStore: TestStore<AppState>) => {
    store = testStore;
    store.setState({ data: [] });
    fixture = TestBed.createComponent(ListHerramientasComponent);
    component = fixture.componentInstance;
    router = TestBed.get(Router);
    location = TestBed.inject(Location);
    fixture.detectChanges();
    queryService = new ListService(httpClient);
  }));
  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('Deberia disparar la accion CargarList in el ngOnInit', () => {
    fixture.detectChanges();
    const action = new CargarList();
    actions = of(action);
    const StoreR = TestBed.get(Store);
    const spy = spyOn(store, 'dispatch');
    StoreR.dispatch();
    expect(spy).toHaveBeenCalled();
    expect(effects).toBeTruthy();
 });
  it('Deberia navegar al login si esta refrescando la pagina y no recordor la sesion in el ngOnInit', fakeAsync( () => {
    localStorage.removeItem('remember');
    component.browserRefresh = true;
    component.ngOnInit();
    tick();
    expect(location.path()).toBe('/login');
  }));
  it('deberia cerrar la sesion, redirijir a login y reinicar los valores de login store', fakeAsync( () => {
    const action = new ReiciarValores();
    actions = of(action);
    const StoreR = TestBed.get(Store);
    const spy = spyOn(store, 'dispatch');
    StoreR.dispatch();
    component.closeSesion();
    expect(spy).toHaveBeenCalled();
    expect(effects).toBeTruthy();
    tick();
    expect(location.path()).toBe('/login');
  }));
  it('Deberia ordenar un array de objectos por el tech, decendente', () => {
      const tecnologias = [
        {
          tech: 'Node',
          year: '2009',
          author: 'Ryan Dahl',
          license: 'MIT',
          language: 'JavaScript',
          type: 'Back-End',
          logo: 'https://upload.wikimedia.org/wikipedia/commons/thumb/d/d9/Node.js_logo.svg/220px-Node.js_logo.svg.png'
        },
        {
          tech: 'React',
          year: '2013',
          author: 'Jordan Walke',
          license: 'MIT',
          language: 'JavaScript',
          type: 'Front-End',
          logo: 'https://upload.wikimedia.org/wikipedia/commons/thumb/a/a7/React-icon.svg/220px-React-icon.svg.png'
        }
      ];
      component.herramientas = tecnologias;
      fixture.detectChanges();
      component.oderDeccendente();
      expect(component.herramientas).toEqual(tecnologias);
  });

  it('Deberia ordenar un array de objectos por el tech, acendentemente', () => {
        const tecnologias = [
          {
            tech: 'Node',
            year: '2009',
            author: 'Ryan Dahl',
            license: 'MIT',
            language: 'JavaScript',
            type: 'Back-End',
            logo: 'https://upload.wikimedia.org/wikipedia/commons/thumb/d/d9/Node.js_logo.svg/220px-Node.js_logo.svg.png'
          },
          {
            tech: 'React',
            year: '2013',
            author: 'Jordan Walke',
            license: 'MIT',
            language: 'JavaScript',
            type: 'Front-End',
            logo: 'https://upload.wikimedia.org/wikipedia/commons/thumb/a/a7/React-icon.svg/220px-React-icon.svg.png'
          }
        ];
        const expectTecnologias = [
          {
            tech: 'Node',
            year: '2009',
            author: 'Ryan Dahl',
            license: 'MIT',
            language: 'JavaScript',
            type: 'Back-End',
            logo: 'https://upload.wikimedia.org/wikipedia/commons/thumb/d/d9/Node.js_logo.svg/220px-Node.js_logo.svg.png'
          },
          {
            tech: 'React',
            year: '2013',
            author: 'Jordan Walke',
            license: 'MIT',
            language: 'JavaScript',
            type: 'Front-End',
            logo: 'https://upload.wikimedia.org/wikipedia/commons/thumb/a/a7/React-icon.svg/220px-React-icon.svg.png'
          }
        ];
        component.herramientas = tecnologias;
        fixture.detectChanges();
        component.oderAccendente();
        expect(component.herramientas).toEqual(expectTecnologias);
    });
});
