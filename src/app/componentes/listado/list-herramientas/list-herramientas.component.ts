import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { NavigationEnd, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { Subscription } from 'rxjs';
import { AppState } from 'src/app/store/app.reducer';
import { CargarList } from 'src/app/store/list/actions/list.actions';
import { ReiciarValores } from 'src/app/store/login/actions/login.actions';

@Component({
  selector: 'app-list-herramientas',
  templateUrl: './list-herramientas.component.html',
  styleUrls: ['./list-herramientas.component.css']
})
export class ListHerramientasComponent implements OnInit , OnDestroy{
  subscription: Subscription;
  searchForm: FormGroup;
  browserRefresh;
  herramientas;
  copiaHerramientas;
  total;
  menuActive = true;

  constructor(private router: Router,  public store: Store<AppState>, private formBuilder: FormBuilder) {
    this.createFormSearch();
    this.subscription = router.events.subscribe((event) => {
      if (event instanceof NavigationEnd) {
         if (event.id === 1){
            this.browserRefresh = true;
         }
      }
  });
  }
  openMenu(): void{
    this.menuActive = !this.menuActive;
  }
  createFormSearch(): void {
    this.searchForm = this.formBuilder.group(
      {
        work: [null],
        typeSearch: ['1']
      }
    );
    this.searchForm.valueChanges.subscribe(data => {
      this.search(data);
     });
  }

  ngOnInit(): void {
    const remember = localStorage.getItem('remember');
    if (this.browserRefresh  && remember  === null) {
         this.router.navigate(['/login']);
    }
    this.store.dispatch(new CargarList());
    this.store.select('list').subscribe(login => {
      if (login){
        if (login.data !== null) {
          const data = [];
          this.herramientas = login.data;
          this.herramientas.forEach((n: any) => {
              data.push({
                ...n,
                show: false
              });
            });
          this.herramientas = data;
          this.copiaHerramientas = [...data];
          this.total = this.herramientas.length;
        }
      }
    });

}
  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  showContent(position): void{
    const data = [];
    this.herramientas.forEach((n: any, i) => {
        if (i === position) {
          n.show = true;
        }
        data.push(n);
      });
    this.herramientas = data;
    this.copiaHerramientas = [...data];
  }

  hideContent(position): void{
    const data = [];
    this.herramientas.forEach((n: any, i) => {
        if (i === position) {
          n.show = false;
        }
        data.push(n);
      });
    this.herramientas = data;
    this.copiaHerramientas = [...data];
  }
  search(data): void{
    if ( data.typeSearch === '1' && data.work !== ''){
      this.herramientas = this.herramientas.filter(h => h.tech.indexOf(data.work) >= 0);
      this.total = this.herramientas.length;
    }
    if ( data.typeSearch === '2' && data.work !== ''){
      this.herramientas = this.herramientas.filter(h => h.type.indexOf(data.work) >= 0);
      this.total = this.herramientas.length;
    }
    if ( data.work === '' ){
      this.herramientas = [...this.copiaHerramientas];
      this.total = this.herramientas.length;
    }
  }
  closeSesion(): void{
    localStorage.removeItem('remember');
    this.store.dispatch(new ReiciarValores());
    this.router.navigate(['login']);
  }
  oderDeccendente(): void{
    console.log(this.herramientas);
    this.herramientas.sort((a, b) => {
        if (a.tech < b.tech) {
          return 1;
        }
        if (a.tech > b.tech) {
          return -1;
        }
        return 0;
      });
  }
  oderAccendente(): void{
    this.herramientas.sort((a, b) => {
        if (a.tech > b.tech) {
          return 1;
        }
        if (a.tech < b.tech) {
          return -1;
        }
        // a must be equal to b
        return 0;
      });
  }
}
