import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListHerramientasComponent } from './list-herramientas/list-herramientas.component';
import { RouterModule, Routes } from '@angular/router';
import { EffectsModule } from '@ngrx/effects';
import { listEffectsArr } from 'src/app/store/list/effects';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

const routes: Routes = [
  { path: 'list', component: ListHerramientasComponent },
];

@NgModule({
  declarations: [ListHerramientasComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    EffectsModule.forRoot(listEffectsArr),
    FormsModule,
    ReactiveFormsModule,
  ]
})
export class ListadoModule { }
