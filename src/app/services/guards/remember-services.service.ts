import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class RememberServicesService implements CanActivate {
  constructor(private router: Router) { }
  canActivate(): boolean {
    const remember = localStorage.getItem('remember');
    if (remember === 'true') {
        this.router.navigate(['/list']);
        return false;
    }
    return true;
  }
}
