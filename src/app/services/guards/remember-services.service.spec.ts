import { getTestBed, TestBed } from '@angular/core/testing';
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { RememberServicesService } from './remember-services.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { LoginComponent } from 'src/app/componentes/login/login/login.component';
import { ListHerramientasComponent } from 'src/app/componentes/listado/list-herramientas/list-herramientas.component';

describe('RememberServicesService', () => {
  let injector: TestBed;
  let service: RememberServicesService;
  const routeMock: any = { snapshot: {}};
  const routeStateMock: any =  { snapshot: {}, url: '/cookies'};
  const routerMock = {navigate: jasmine.createSpy('navigate')};

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [RememberServicesService, { provide: Router, useValue: routerMock }],
      imports: [
        HttpClientTestingModule,
        RouterTestingModule,
      ],
      declarations: [
        ListHerramientasComponent,
        LoginComponent
      ]
    });

    injector = getTestBed();
    service = injector.inject(RememberServicesService);
  });
  it('should be created', () => {
    const service: RememberServicesService = TestBed.inject(RememberServicesService);
    expect(service).toBeTruthy();
  });
  it('Debe retornar true porque la sesion no esta recordada', () => {
    localStorage.removeItem('remember');
    expect(service.canActivate()).toEqual(true);
  });
  it('Debe redirijir al lista de tecnologias porque recuerda la sesion', () => {
    localStorage.setItem('remember', 'true');
    service.canActivate();
    expect(routerMock.navigate).toHaveBeenCalledWith(['/list']);
  });
});
