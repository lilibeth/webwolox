import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { getTestBed, inject, TestBed } from '@angular/core/testing';
import { environment } from 'src/environments/environment';
import { ListService } from './list.service';

describe('ListService', () => {
  let service: ListService;
  let injector: TestBed;
  let httpMock: HttpTestingController;
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ]
      ,
      providers: [
        ListService
      ]
    });
    injector = getTestBed();
    httpMock = injector.inject(HttpTestingController);
    service = TestBed.inject(ListService);
  });
  it('should be created', () => {
    expect(service).toBeTruthy();
  });
  it('Debe obtener los datos de techs',  inject([ListService], (service: ListService) => {
    const mockResponse = [
      {
        tech: 'Node',
        year: '2009',
        author: 'Ryan Dahl',
        license: 'MIT',
        language: 'JavaScript',
        type: 'Back-End',
        logo: 'https://upload.wikimedia.org/wikipedia/commons/thumb/d/d9/Node.js_logo.svg/220px-Node.js_logo.svg.png'
      }
    ];
    const url = environment.url + '/techs';
    service.getList().subscribe(result=>{
      expect(result[0].tech).toEqual('Node');
    });
    httpMock.expectOne(url).flush(mockResponse);
  }));
});
