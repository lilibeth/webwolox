import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ListService {
  private apiUrl: string = environment.url;
  httpHeaders: HttpHeaders;
  constructor( private http: HttpClient){
    this.httpHeaders = new HttpHeaders({
      'Content-type': 'application/json'
    });
  }
  getList(): any{
    return this.http.get(this.apiUrl + '/techs', {headers: this.httpHeaders});
  }
}
