import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  private apiUrl: string = environment.url;
  httpHeaders: HttpHeaders;
  constructor( private http: HttpClient) {
    this.httpHeaders = new HttpHeaders({
      'Content-type': 'application/json'
    });
  }
  getLogin(data: any): any{
    return this.http.post(this.apiUrl + '/login', data, {headers: this.httpHeaders});
  }
}
