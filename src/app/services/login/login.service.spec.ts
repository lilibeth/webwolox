import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { getTestBed, inject, TestBed } from '@angular/core/testing';
import { environment } from 'src/environments/environment';
import { LoginService } from './login.service';

describe('LoginService', () => {
    let service: LoginService;
    let injector: TestBed;
    let httpMock: HttpTestingController;
    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [
          HttpClientTestingModule
        ],
        providers: [
            LoginService
        ]
      });
      injector = getTestBed();
      httpMock = injector.inject(HttpTestingController);
      service = TestBed.inject(LoginService);
    });
    it('should be created', () => {
      expect(service).toBeTruthy();
    });
    it('Debe obtener los datos de login',  inject([LoginService], (service: LoginService) => {
      const mockResponse = {
        token: 'qiowAS9ndnjLKSS32LaLAPlDKL2'
      };
      const url = environment.url + '/login';
      service.getLogin({}).subscribe(result => {
        expect(result.token).toEqual('qiowAS9ndnjLKSS32LaLAPlDKL2');
      });
      httpMock.expectOne(url).flush(mockResponse);
    }));
});
